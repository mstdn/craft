# Craft - Survival MMO Unreal Engine 5.0.x

Welcome to the Craft repository, on here you will find all project files and assets needed to compile the Craft MMO game. This game is still very much in development and not ready for production. Additions and updates will be added to this repo as we go!


## Features roadmap

- [x] Inventory System framework
- [x] Player Hotbar & Tool Equipables
- [x] Harvesting System
- [x] Crafting System
- [x] Player Stats
- [x] Armor Equipables
- [ ] Building System
- [ ] Storage Containers
- [ ] Adding items
- [ ] Tribes / Clans System
- [ ] Social System
- [ ] Open World Map
- [ ] AI System
- [ ] Zones
- [ ] Player Minimap
- [ ] Save & Load System
- [ ] Front-end Widgets

## Known bugs

- [ ] Buildable snapping in distance (DistanceFar)

#### Quick start

Download the latest source code,unzip and open the 'Craft.uproject' to launch the project in Unreal Engine. Before you press play makke sure to set the Net Mode to 'Play As Client' since this is a server/client game. 
 